<?php

namespace TerryFriday\LaravelSupport\FormRequest;


use Illuminate\Contracts\Validation\ValidatesWhenResolved;
use Illuminate\Support\Facades\Validator;
use Laravel\Lumen\Application;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Http\Request as IlluminateRequest;

class FormRequestServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //手机号码验
        Validator::extend('mobile', function($attribute, $value, $parameters, $validator) {
            if(preg_match("/^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(16[0-9]{1})|(17[0-9]{1})|(19[0-9]{1})|(18[0-9]{1}))+\d{8})$/",$value)) return true;

            return preg_match("/^0\d{2,3}[1-9]\d{6,8}$/",$value);
        });


        $this->app->afterResolving(ValidatesWhenResolved::class, function ($resolved) {
            $resolved->validateResolved();
        });

        $this->app->resolving(FormRequest::class, function (FormRequest $request, Application $app) {
            $this->initializeRequest($request, $app['request']);
            $request->setContainer($app);
        });
    }

    public function register()
    {

    }

    protected function initializeRequest(FormRequest $form, IlluminateRequest $current)
    {
        $files = $current->files->all();

        $files = is_array($files) ? array_filter($files) : $files;

        $form->initialize(
            $current->query->all(),
            $current->request->all(),
            $current->attributes->all(),
            $current->cookies->all(),
            $files,
            $current->server->all(),
            $current->getContent()
        );

        $form->setJson($current->json());

        if ($session = $current->getSession()) {
            $form->setLaravelSession($session);
        }

        $form->setUserResolver($current->getUserResolver());

        $form->setRouteResolver($current->getRouteResolver());
    }
}
